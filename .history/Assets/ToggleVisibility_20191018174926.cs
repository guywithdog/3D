﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleVisibility : MonoBehaviour
{
    public Toggle layerToggle;
    // Start is called before the first frame update
    void Start()
    {
        layerToggle = this.GetComponent<Toggle>();
    }
    public void UpdateVisibility()
    {
        Shader.SetGlobalInt(layerToggle.gameObject.name.Substring(6) + "_On", layerToggle.isOn?1:0);
    }
}
