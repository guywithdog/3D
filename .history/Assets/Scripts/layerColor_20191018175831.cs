﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityEngine{
public class layerColor : MonoBehaviour
{

    public GameObject thumbToChange;
    public string layerToChange;
    public string newColor = "";
    public string oldColor = "";
    public Color startColor;
    public Texture2D[] layers = new Texture2D[19];
    public void changeColor(string newName){
        newColor = newName;
    }
    public void ChangeDisp(string layerToChange){
        Shader.SetGlobalTexture("DispMap", thumbToChange.GetComponent<Renderer>().material.mainTexture);
    }
    void Start(){
        thumbToChange.GetComponent<Renderer>().material.color = startColor;
        layerToChange = thumbToChange.name.Substring(0, thumbToChange.name.Length - 7).Replace(" ", "_");
        Shader.SetGlobalFloatArray(layerToChange + "_Color", new float[]{startColor.r, startColor.g, startColor.b, startColor.a});
    }
    void Update(){
        if(!oldColor.Equals(newColor)){
            Color colorToChangeTo = new Color(0.0f, 0.0f, 0.0f);
            if(newColor.Equals("red")){
                colorToChangeTo = new Color(1.0f, 0.0f, 0.0f, 1.0f);
            } else if(newColor.Equals("yellow")){
                colorToChangeTo = new Color(1.0f, 1.0f, 0.0f, 1.0f);
            } else if(newColor.Equals("green")){
                colorToChangeTo = new Color(0.0f, 1.0f, 0.0f, 1.0f);
            } else if(newColor.Equals("cyan")){
                colorToChangeTo = new Color(0.0f, 1.0f, 1.0f, 1.0f);
            } else if(newColor.Equals("blue")){
                colorToChangeTo = new Color(0.0f, 0.0f, 1.0f, 1.0f);
            } else if(newColor.Equals("magenta")){
                colorToChangeTo = new Color(1.0f, 0.0f, 1.0f, 1.0f);
            }
            
            thumbToChange.GetComponent<Renderer>().material.color = colorToChangeTo;
            Shader.SetGlobalFloatArray(layerToChange + "_Color", new float[]{colorToChangeTo.r, colorToChangeTo.g, colorToChangeTo.b, colorToChangeTo.a});
            oldColor = newColor;
        }
    }
}
}