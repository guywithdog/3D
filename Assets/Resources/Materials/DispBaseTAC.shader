// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X
Shader "DispBaseTAC"
{
	Properties
	{
		_TessValue( "Max Tessellation", Range( 1, 32 ) ) = 32
		_TessMin( "Tess Min Distance", Float ) = 10
		_TessMax( "Tess Max Distance", Float ) = 25
		_TessPhongStrength( "Phong Tess Strength", Range( 0, 1 ) ) = 0.5
		// _DAPI_R2("DAPI R2", 2D) = "black" {}
		// _CD44("CD44", 2D) = "black" {}
		// _PD1("PD1", 2D) = "black" {}
		// _DAPI_R3("DAPI R3", 2D) = "black" {}
		// _Her2("Her2", 2D) = "black" {}
		// _DAPI_R4("DAPI R4", 2D) = "black" {}
		// _DAPI_R5("DAPI R5", 2D) = "black" {}
		// _Ki67("Ki67", 2D) = "black" {}
		// _PH3("PH3", 2D) = "black" {}
		// _CK5("CK5", 2D) = "black" {}
		// _PCNA("PCNA", 2D) = "black" {}
		// _CD45("CD45", 2D) = "black" {}
		// _LaminAC("LaminAC", 2D) = "black" {}
		// _CD68("CD68", 2D) = "black" {}
		// _Vim("Vim", 2D) = "black" {}
		// _ER("ER", 2D) = "black" {}
		// DispMap("Disp Map", 2D) = "black" {}
		// DispMap_Scale("Disp Map Scale", Range(0.001, 1)) = 0.5
		// _aSmA("aSmA", 2D) = "black" {}
		[HideInInspector] _Black("Black", 2D) = "black" {}
		// _CK14("CK14", 2D) = "black" {}
		// _Ecad("Ecad", 2D) = "black" {}
		[HideInInspector] _Boys("Boys", 2DArray) = "" {}
		// DispMap_On("DispMap_On", Float) = 0
		// [HideInInspector] DAPI_R2_On("DAPI_R2_On", Float) = 1
		// [HideInInspector] PCNA_On("PCNA_On", Float) = 0
		// [HideInInspector] Her2_On("Her2_On", Float) = 0
		// [HideInInspector] ER_On("ER_On", Float) = 0
		// [HideInInspector] DAPI_R3_On("DAPI_R3_On", Float) = 1
		// [HideInInspector] CD45_On("CD45_On", Float) = 0
		// [HideInInspector] Ki67_On("Ki67_On", Float) = 0
		// [HideInInspector] CK14_On("CK14_On", Float) = 0
		// [HideInInspector] CD68_On("CD68_On", Float) = 0
		// [HideInInspector] PH3_On("PH3_On", Float) = 0
		// [HideInInspector] Ecad_On("Ecad_On", Float) = 0
		// [HideInInspector] CD44_On("CD44_On", Float) = 0
		// [HideInInspector] CK5_On("CK5_On", Float) = 0
		// [HideInInspector] DAPI_R4_On("DAPI_R4_On", Float) = 1
		// [HideInInspector] Vim_On("Vim_On", Float) = 0
		// [HideInInspector] PD1_On("PD1_On", Float) = 0
		// [HideInInspector] LaminAC_On("LaminAC_On", Float) = 0
		// [HideInInspector] DAPI_R5_On("DAPI_R5_On", Float) = 1
		// [HideInInspector] aSmA_On("aSmA_On", Float) = 0
		// DAPI_R2_Color("DAPI_R2_Color", Color) = (0, 1, 1, 1)
		// [HideInInspector] PCNA_Color("PCNA_Color", Color) = (0, 1, 1, 1)
		// [HideInInspector] Her2_Color("Her2_Color", Color) = (1, 1, 0, 1)
		// [HideInInspector] ER_Color("ER_Color", Color) = (1, 0, 0, 1)
		// [HideInInspector] DAPI_R3_Color("DAPI_R3_Color", Color) = (0, 0, 1, 1)
		// [HideInInspector] CD45_Color("CD45_Color", Color) = (1, 0, 1, 1)
		// [HideInInspector] Ki67_Color("Ki67_Color", Color) = (0, 1, 0, 1)
		// [HideInInspector] CK14_Color("CK14_Color", Color) = (1, 1, 0, 1)
		// [HideInInspector] CD68_Color("CD68_Color", Color) = (0, 0, 1, 1)
		// [HideInInspector] PH3_Color("PH3_Color", Color) = (1, 0, 0, 1)
		// [HideInInspector] Ecad_Color("Ecad_Color", Color) = (0, 0, 1, 1)
		// [HideInInspector] CD44_Color("CD44_Color", Color) = (1, 0, 0, 1)
		// [HideInInspector] CK5_Color("CK5_Color", Color) = (1, 0, 1, 1)
		// [HideInInspector] DAPI_R4_Color("DAPI_R4_Color", Color) = (0, 1, 1, 1)
		// [HideInInspector] Vim_Color("Vim_Color", Color) = (0, 1, 0, 1)
		// [HideInInspector] PD1_Color("PD1_Color", Color) = (0, 0, 1, 1)
		// [HideInInspector] LaminAC_Color("LaminAC_Color", Color) = (1, 0, 1, 1)
		// [HideInInspector] DAPI_R5_Color("DAPI_R5_Color", Color) = (1, 1, 0, 1)
		// [HideInInspector] aSmA_Color("aSmA_Color", Color) = (0, 0, 1, 1)
		[HideInInspector] _texcoord( "", 2D ) = "black" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull off
		CGPROGRAM
		#include "Tessellation.cginc"
		#pragma target 5.0
		#pragma exclude_renderers vulkan xbox360 xboxone ps4 psp2 n3ds wiiu
		#pragma surface surf Lambert keepalpha addshadow fullforwardshadows noshadow vertex:vertexDataFunc tessellate:tessFunction tessphong:_TessPhongStrength
		#pragma require 2darray
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float DispMap_On;
		uniform sampler2D DispMap;
		uniform float4 DispMap_ST;
		uniform float DispMap_Scale;

		uniform float DAPI_R2_On;
		uniform float4 DAPI_R2_Color;
		// uniform sampler2D _DAPI_R2;


		uniform float PCNA_On;
		uniform float4 PCNA_Color;
		// uniform sampler2D _PCNA;


		uniform float Her2_On;
		uniform float4 Her2_Color;
		// uniform sampler2D _Her2;


		uniform float ER_On;
		uniform float4 ER_Color;
		// uniform sampler2D _ER;


		uniform float DAPI_R3_On;
		uniform float4 DAPI_R3_Color;
		// uniform sampler2D _DAPI_R3;


		uniform float CD45_On;
		uniform float4 CD45_Color;
		// uniform sampler2D _CD45;


		uniform float Ki67_On;
		uniform float4 Ki67_Color;
		// uniform sampler2D _Ki67;


		uniform float CK14_On;
		uniform float4 CK14_Color;
		// uniform sampler2D _CK14;


		uniform float CD68_On;
		uniform float4 CD68_Color;
		// uniform sampler2D _CD68;


		uniform float PH3_On;
		uniform float4 PH3_Color;
		// uniform sampler2D _PH3;


		uniform float Ecad_On;
		uniform float4 Ecad_Color;
		// uniform sampler2D _Ecad;


		uniform float CD44_On;
		uniform float4 CD44_Color;
		// uniform sampler2D _CD44;


		uniform float CK5_On;
		uniform float4 CK5_Color;
		// uniform sampler2D _CK5;


		uniform float DAPI_R4_On;
		uniform float4 DAPI_R4_Color;
		// uniform sampler2D _DAPI_R4;


		uniform float Vim_On;
		uniform float4 Vim_Color;
		// uniform sampler2D _Vim;


		uniform float PD1_On;
		uniform float4 PD1_Color;
		// uniform sampler2D _PD1;


		uniform float LaminAC_On;
		uniform float4 LaminAC_Color;
		// uniform sampler2D _LaminAC;


		uniform float DAPI_R5_On;
		uniform float4 DAPI_R5_Color;
		// uniform sampler2D _DAPI_R5;


		uniform float aSmA_On;
		uniform float4 aSmA_Color;
		// uniform sampler2D _aSmA;


		uniform float _TessValue;
		uniform float _TessMin;
		uniform float _TessMax;
		uniform float _TessPhongStrength;
		UNITY_DECLARE_TEX2DARRAY(_Boys);

		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0f, -1.0f / 3.0f, 2.0f / 3.0f, -1.0f);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0f * d + e)), d / (q.x + e), q.x);
		}

		float4 tessFunction( appdata_full v0, appdata_full v1, appdata_full v2 )
		{
			return UnityDistanceBasedTess( v0.vertex, v1.vertex, v2.vertex, _TessMin, _TessMax, _TessValue );
		}


		void vertexDataFunc( inout appdata_full v )
		{
			float2 uv_DispMap = v.texcoord* (DispMap_ST.xy + DispMap_ST.zw);
			float3 hsvTorgb2 = RGBToHSV( (DispMap_On * DispMap_Scale * tex2Dlod( DispMap, float4( uv_DispMap, 0, 0.0f) )).rgb );
			float3 temp_cast_1 = (hsvTorgb2.z).xxx;
			v.vertex.xyz += hsvTorgb2.z * v.normal;
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float3 uv_DAPI_R2 = float3(i.uv_texcoord, 0);
			float3 uv_PCNA = float3(i.uv_texcoord, 1);
			float3 uv_Her2 = float3(i.uv_texcoord, 2);
			float3 uv_ER = float3(i.uv_texcoord, 3);
			float3 uv_DAPI_R3 = float3(i.uv_texcoord, 4);
			float3 uv_CD45 = float3(i.uv_texcoord, 5);
			float3 uv_Ki67 = float3(i.uv_texcoord, 6);
			float3 uv_CK14 = float3(i.uv_texcoord, 7);
			float3 uv_CD68 = float3(i.uv_texcoord, 8);
			float3 uv_PH3 = float3(i.uv_texcoord, 9);
			float3 uv_Ecad = float3(i.uv_texcoord, 10);
			float3 uv_CD44 = float3(i.uv_texcoord, 11);
			float3 uv_CK5 = float3(i.uv_texcoord, 12);
			float3 uv_DAPI_R4 = float3(i.uv_texcoord, 13);
			float3 uv_Vim = float3(i.uv_texcoord, 14);
			float3 uv_PD1 = float3(i.uv_texcoord, 15);
			float3 uv_LaminAC = float3(i.uv_texcoord, 16);
			float3 uv_DAPI_R5 = float3(i.uv_texcoord, 17);
			float3 uv_aSmA = float3(i.uv_texcoord, 18);
			o.Emission = ((((DAPI_R2_On * DAPI_R2_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_DAPI_R2).rgb)  + (PCNA_On * PCNA_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_PCNA).rgb)) +  ((Her2_On * Her2_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_Her2).rgb)  + (ER_On * ER_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_ER).rgb)))  + (((DAPI_R3_On * DAPI_R3_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_DAPI_R3).rgb) + (CD45_On * CD45_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_CD45).rgb))  + ((Ki67_On * Ki67_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_Ki67).rgb) + (CK14_On * CK14_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_CK14).rgb))))  + (((((CD68_On * CD68_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_CD68).rgb)  + (PH3_On * PH3_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_PH3).rgb)) + ((Ecad_On * Ecad_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_Ecad).rgb)  + (CD44_On * CD44_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_CD44).rgb))) + (((CK5_On * CK5_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_CK5).rgb) + (DAPI_R4_On * DAPI_R4_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_DAPI_R4).rgb)) + ((Vim_On * Vim_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_Vim).rgb)  + (PD1_On * PD1_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_PD1).rgb)))) + (((LaminAC_On * LaminAC_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_LaminAC).rgb) + (DAPI_R5_On * DAPI_R5_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_DAPI_R5).rgb)) + (aSmA_On * aSmA_Color.xyz * UNITY_SAMPLE_TEX2DARRAY(_Boys, uv_aSmA).rgb)));
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	// CustomEditor "ASEMaterialInspector"
}
