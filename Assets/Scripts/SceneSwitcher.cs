﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    void OnEnable()
    {
        Debug.Log("OnEnable called");
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    public void SwitchScene(){
        if(SceneManager.GetActiveScene().name.Equals("T-ATAC3A-1LCore37")){
            SceneManager.LoadScene("HumanTumorAtlas");
        } else if(SceneManager.GetActiveScene().name.Equals("HumanTumorAtlas")){
            SceneManager.LoadScene("T-ATAC3A-1LCore37");
        }
    }
    void OnSceneLoaded(Scene scene, LoadSceneMode mode){
        SceneManager.SetActiveScene(scene);
    }
}
