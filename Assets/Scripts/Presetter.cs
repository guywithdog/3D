﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Presetter : MonoBehaviour
{
    public Transform head;
    public Transform specimenPlane;
    public Transform specimenContainer;
    public Toggle DispToggle;
    public Slider DispScale;
    public Texture2D DispLayer;
    public Toggle gridToggle;
    public Slider specScale;
    public Transform plotContainer;
    public Transform plot;
    public Slider labelSize;
    public GameObject tutorial;
    void Start()
    {
        gridToggle.isOn = false;
        specimenContainer.localEulerAngles = new Vector3(-90f, 0f, 0f);
        specimenContainer.localScale = new Vector3(1.086f * 0.2f, 0.2f, 0.2f);
        specimenContainer.localPosition = new Vector3(0f, 1f, 1f);
        Shader.SetGlobalTexture("DispMap", DispLayer);
        DispToggle.isOn = true;
        DispScale.value = 0.75f;
        Shader.SetGlobalFloat("_TessValue", 3f);
        Shader.SetGlobalFloat("_TessMin", -4f);
        Shader.SetGlobalFloat("_TessMax", 40f);
    }
    public void MountainPreset(){
        gridToggle.isOn = false;
        specimenContainer.localEulerAngles = new Vector3(0f, 0f, 0f);
        specimenContainer.localScale = new Vector3(10.86f, 5.0f, 10.0f);
        specimenContainer.localPosition = new Vector3(33.1f, -2f, 25.9f);
        Shader.SetGlobalTexture("DispMap", DispLayer);
        DispToggle.isOn = true;
        DispScale.value = 0.75f;
        Shader.SetGlobalFloat("_TessValue", 10f);
        Shader.SetGlobalFloat("_TessMin", -4f);
        Shader.SetGlobalFloat("_TessMax", 40f);
    }
    public void WallPreset(){
        gridToggle.isOn = false;
        specimenContainer.localEulerAngles = new Vector3(90f, 0f, 0f);
        specimenContainer.localScale = new Vector3(10.86f, 5.0f, 10.0f);
        specimenContainer.localPosition = new Vector3(0.255f, -1.08f, -12.66f);
        Shader.SetGlobalTexture("DispMap", DispLayer);
        DispToggle.isOn = true;
        DispScale.value = 1.5f;
        Shader.SetGlobalFloat("_TessValue", 7f);
        Shader.SetGlobalFloat("_TessMin", 30f);
        Shader.SetGlobalFloat("_TessMax", 45f);
    }
    public void DefaultPreset(){
        gridToggle.isOn = true;
        specimenContainer.localScale = new Vector3(0.1086f, 0.1f, 0.1f);
        specimenContainer.position = head.position + head.TransformDirection(new Vector3(0, 0, 0.75f));
        specimenContainer.localEulerAngles = new Vector3(180f, head.localEulerAngles.y + 90f, 90f - head.localEulerAngles.z);
        Debug.Log("default implemented");
        DispScale.value = 0.75f;
        Shader.SetGlobalFloat("_TessValue", 3f);
        Shader.SetGlobalFloat("_TessMin", -4f);
        Shader.SetGlobalFloat("_TessMax", 40f);
    }
    public void TablePreset(){
        gridToggle.isOn = false;
        specimenContainer.localEulerAngles = new Vector3(0f, 0f, 0f);
        specimenContainer.localScale = new Vector3(1.086f * 0.1507303f, 0.1507303f, 0.1507303f);
        specimenContainer.localPosition = new Vector3(0.075f, 1.017569f, -0.837f);
        Shader.SetGlobalTexture("DispMap", DispLayer);
        DispToggle.isOn = true;
        DispScale.value = 0.75f;
        Shader.SetGlobalFloat("_TessValue", 3f);
        Shader.SetGlobalFloat("_TessMin", -4f);
        Shader.SetGlobalFloat("_TessMax", 40f);
    }
    public void CityPreset(){
        gridToggle.isOn = false;
        specimenContainer.localEulerAngles = new Vector3(0f, 0f, 0f);
        specimenContainer.localScale = new Vector3(1.086f * 0.3248747f, 0.3248747f, 0.3248747f);
        specimenContainer.localPosition = new Vector3(0.4062445f, 1.157758f, 0.2611572f);
        Shader.SetGlobalTexture("DispMap", DispLayer);
        DispToggle.isOn = true;
        DispScale.value = 0.75f;
        Shader.SetGlobalFloat("_TessValue", 3f);
        Shader.SetGlobalFloat("_TessMin", -4f);
        Shader.SetGlobalFloat("_TessMax", 40f);
    }
    void OnGUI()
    {
        if (Event.current.Equals(Event.KeyboardEvent(KeyCode.Space.ToString())))
        {
            DefaultPreset();
        } else if(Event.current.Equals(Event.KeyboardEvent(KeyCode.F.ToString()))){
            if(gridToggle.isOn){
                gridToggle.isOn = false;
            } else {
                gridToggle.isOn = true;
            }
        } else if(Event.current.Equals(Event.KeyboardEvent(KeyCode.H.ToString()))){
            if(DispToggle.isOn){
                DispToggle.isOn = false;
            } else {
                DispToggle.isOn = true;
                DispScale.value = 1.0f;
            }
        } else if(Event.current.Equals(Event.KeyboardEvent(KeyCode.T.ToString()))){
            tutorial.SetActive(true);
        }
    }
    public void ScatterDefaultPreset(){
        plot.SetParent(plotContainer);
        plot.localScale = Vector3.one;
        plot.localPosition = Vector3.zero;
        plot.localEulerAngles = Vector3.zero;
        labelSize.value = 0.0075f;
    }
    public void ScatterLargePreset(){
        plot.SetParent(null);
        plot.localScale = new Vector3(8.240803f, 8.240803f, 8.240803f);
        plot.localPosition = new Vector3(-1.0485f, 0.01657975f, 1.601956f);
        plot.localEulerAngles = new Vector3(0f, 180f, 0f);
        labelSize.value = 0.02f;
    }
    public void ReclinePreset(){
        specimenContainer.localScale = new Vector3(1.086f * 0.2033f, 0.2033f, 0.2033f);
        specimenContainer.localEulerAngles = new Vector3(180f, 80f, 60f);
        specimenContainer.localPosition = new Vector3(0.06f, 1.85f, -0.8f);
        specScale.value = 1;
        gridToggle.isOn = true;
        Shader.SetGlobalFloat("_TessValue", 3f);
        Shader.SetGlobalFloat("_TessMin", -4f);
        Shader.SetGlobalFloat("_TessMax", 40f);
    }
}
