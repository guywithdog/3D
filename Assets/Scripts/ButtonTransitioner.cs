﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonTransitioner : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public Color32 m_NormalColor = Color.white;
    public Color32 m_HoverColor = Color.grey;
    public Color32 m_DownColor = Color.white;
    private Image m_Image = null;

    private void Awake(){
        m_Image = GetComponent<Image>();
    }
    public void OnPointerEnter(PointerEventData e){
        m_Image.color = m_HoverColor;
    }
    public void OnPointerExit(PointerEventData e){
        m_Image.color = m_NormalColor;
    }
    public void OnPointerDown(PointerEventData e){
        m_Image.color = m_DownColor;
    }
    public void OnPointerUp(PointerEventData e){
    }
    public void OnPointerClick(PointerEventData e){
        m_Image.color = m_HoverColor;
    }
}
