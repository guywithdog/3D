﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlotSpecScaler : MonoBehaviour
{
    public Slider specSlider;
    public Transform specimen;
    public Slider plotSlider;
    public Transform plot;
    public bool plotDragging = false;
    public bool specDragging = false;
    void Start()
    {
        specSlider.value = specimen.localScale.y;
        plot.localScale = Vector3.one;
        plotSlider.value = plot.localScale.y;
    }
    // Update is called once per frame
    void Update()
    {
        if (!specDragging)
        {
            if(specimen.localScale.y < specSlider.maxValue && specimen.localScale.y > specSlider.minValue){
                specSlider.value = specimen.localScale.y;
            } else if(specimen.localScale.y >= specSlider.maxValue && specSlider.value != specSlider.maxValue){
                specSlider.value = specSlider.maxValue;
            } else if(specimen.localScale.y <= specSlider.minValue && specSlider.value != specSlider.minValue){
                specSlider.value = specSlider.minValue;
            }
        } else if(specDragging){
            specimen.localScale = new Vector3(specSlider.value * 1.086f, specSlider.value, specSlider.value);
        }
        if (!plotDragging)
        {
            if(plot.localScale.y < plotSlider.maxValue && plot.localScale.y > plotSlider.minValue){
                plotSlider.value = plot.localScale.y;
            } else if(plot.localScale.y >= plotSlider.maxValue && plotSlider.value != plotSlider.maxValue){
                plotSlider.value = plotSlider.maxValue;
            } else if(plot.localScale.y <= plotSlider.minValue && plotSlider.value != plotSlider.minValue){
                plotSlider.value = plotSlider.minValue;
            }
        } else if(plotDragging && plot.parent == null){
            plot.localScale = new Vector3(plotSlider.value, plotSlider.value, plotSlider.value);
        }

    }
    public void dragPlot(BaseEventData data){
        plotDragging = true;
    }
    public void noDragPlot(BaseEventData data){
        plotDragging = false;
    }
    public void dragSpec(BaseEventData data){
        specDragging = true;
    }
    public void noDragSpec(BaseEventData data){
        specDragging = false;
    }
}
