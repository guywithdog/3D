﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using LumenWorks.Framework.IO.Csv;
using System.IO;
public class CSVReader : MonoBehaviour {

    public static List<Dictionary<string, object>> Read (string file) {

        // open the file "data.csv" which is a CSV file with headers
        TextAsset info = Resources.Load<TextAsset>(file);
        List<Dictionary<string, object>> list = new List<Dictionary<string, object>> ();
        using (CsvReader csv = new CsvReader (new StreamReader(new MemoryStream(info.bytes)), true)) {
            int fieldCount = csv.FieldCount;

            string[] headers = csv.GetFieldHeaders ();
            while (csv.ReadNextRecord ()) {
                var entry = new Dictionary<string, object> ();
                string[] values = new string[fieldCount];
                csv.CopyCurrentRecordTo(values);
                for (int i = 0; i < fieldCount; i++){
                    entry[headers[i]] = values[i];
                }
                list.Add(entry);
            }
        }
        return list;
        // var list = new List<Dictionary<string, object>> ();
        // TextAsset data = Resources.Load (file) as TextAsset;

        // var lines = Regex.Split (data.text, LINE_SPLIT_RE);

        // if (lines.Length <= 1) return list;

        // var header = Regex.Split (lines[0], SPLIT_RE);
        // for (var i = 1; i < lines.Length; i++) {

        //     var values = Regex.Split (lines[i], SPLIT_RE);
        //     if (values.Length == 0 || values[0] == "") continue;

        //     var entry = new Dictionary<string, object> ();
        //     for (var j = 0; j < header.Length && j < values.Length; j++) {
        //         string value = values[j];
        //         value = value.TrimStart (TRIM_CHARS).TrimEnd (TRIM_CHARS).Replace ("\\", "");
        //         object finalvalue = value;
        //         int n;
        //         float f;
        //         if (int.TryParse (value, out n)) {
        //             finalvalue = n;
        //         } else if (float.TryParse (value, out f)) {
        //             finalvalue = f;
        //         }
        //         entry[header[j]] = finalvalue;
        //     }
        //     list.Add (entry);
        // }
        // return list;

    }
}