﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace stringExtended{
    using static SubstringExtended;
    public class layerColor : MonoBehaviour
    {

        public GameObject thumbToChange;

        public string layerToChange;
        public string newColor = "";
        public string oldColor = "";

        public Color startColor;
        public void changeColor(string newName){
            newColor = newName;
            Color colorToChangeTo = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            if(newColor.Equals("red")){
                colorToChangeTo = new Color(1.0f, 0.0f, 0.0f, 1.0f);
            } else if(newColor.Equals("yellow")){
                colorToChangeTo = new Color(1.0f, 1.0f, 0.0f, 1.0f);
            } else if(newColor.Equals("green")){
                colorToChangeTo = new Color(0.0f, 1.0f, 0.0f, 1.0f);
            } else if(newColor.Equals("cyan")){
                colorToChangeTo = new Color(0.0f, 1.0f, 1.0f, 1.0f);
            } else if(newColor.Equals("blue")){
                colorToChangeTo = new Color(0.0f, 0.0f, 1.0f, 1.0f);
            } else if(newColor.Equals("magenta")){
                colorToChangeTo = new Color(1.0f, 0.0f, 1.0f, 1.0f);
            }

            thumbToChange.GetComponent<Renderer>().material.color = colorToChangeTo;
            Shader.SetGlobalColor(layerToChange + "_Color", colorToChangeTo);
            Debug.Log(layerToChange + "_Color: " + Shader.GetGlobalColor(layerToChange + "_Color"));
            oldColor = newColor;

        }
        public void ChangeDisp(string layerToChange){
            Shader.SetGlobalTexture("DispMap", thumbToChange.GetComponent<Renderer>().material.mainTexture);
            Debug.Log("DispMap: " + Shader.GetGlobalTexture("DispMap"));
        }
        void Start(){
            // List<string> colorList = new List<string>(){"yellow", "green", "cyan"};
            thumbToChange.GetComponent<Renderer>().material.color = startColor;
            layerToChange = thumbToChange.name.SubstringPlus(0, -6).Replace(" ", "_");
            // changeColor(colorList[new System.Random().Next(colorList.Count)]);
            Shader.SetGlobalColor(layerToChange + "_Color", startColor);
            Debug.Log(layerToChange + "_Color: " + Shader.GetGlobalColor(layerToChange + "_Color"));

        }
        void Update()
        {
            if(!oldColor.Equals(newColor)){
                Color colorToChangeTo = new Color(0.0f, 0.0f, 0.0f);
                if(newColor.Equals("red")){
                    colorToChangeTo = new Color(1.0f, 0.0f, 0.0f, 1.0f);
                } else if(newColor.Equals("yellow")){
                    colorToChangeTo = new Color(1.0f, 1.0f, 0.0f, 1.0f);
                } else if(newColor.Equals("green")){
                    colorToChangeTo = new Color(0.0f, 1.0f, 0.0f, 1.0f);
                } else if(newColor.Equals("cyan")){
                    colorToChangeTo = new Color(0.0f, 1.0f, 1.0f, 1.0f);
                } else if(newColor.Equals("blue")){
                    colorToChangeTo = new Color(0.0f, 0.0f, 1.0f, 1.0f);
                } else if(newColor.Equals("magenta")){
                    colorToChangeTo = new Color(1.0f, 0.0f, 1.0f, 1.0f);
                }

                thumbToChange.GetComponent<Renderer>().material.color = colorToChangeTo;
                Shader.SetGlobalColor(layerToChange + "_Color", colorToChangeTo);
                Debug.Log(layerToChange + "_Color: " + Shader.GetGlobalColor(layerToChange + "_Color"));
                oldColor = newColor;
            }
        }

    }
}
