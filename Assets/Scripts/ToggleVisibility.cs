﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleVisibility : MonoBehaviour
{
    public Toggle layerToggle;
    // Start is called before the first frame update
    void Start()
    {
        layerToggle = this.GetComponent<Toggle>();
        Shader.SetGlobalFloat(layerToggle.gameObject.name.Substring(6) + "_On", layerToggle.isOn?1.0f:0.0f);
    }
    public void UpdateVisibility()
    {
        Shader.SetGlobalFloat(layerToggle.gameObject.name.Substring(6) + "_On", layerToggle.isOn?1.0f:0.0f);
        Debug.Log(layerToggle.gameObject.name.Substring(6) + "_On: " + Shader.GetGlobalFloat(layerToggle.gameObject.name.Substring(6) + "_On"));
    }
}
